package com.wahyupermadie.feature_home_api.dagger

import com.wahyupermadie.feature_home_api.dagger.contract.HomeApiContract
import com.wahyupermadie.feature_home_api.dagger.module.HomeApiModule
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = [HomeApiModule::class])
interface HomeApiComponent {
    fun homeApiContract(): HomeApiContract
}