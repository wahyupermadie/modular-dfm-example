package com.wahyupermadie.feature_home_api.dagger.factory

import com.wahyupermadie.feature_home_api.dagger.contract.HomeApiContract

class EmptyHomeApiFactory: HomeApiContract {
    override fun createStringName(): String {
        return ""
    }
}