package com.wahyupermadie.feature_home_api.dagger.module

import com.wahyupermadie.feature_home_api.dagger.contract.HomeApiContract
import com.wahyupermadie.feature_home_api.dagger.factory.EmptyHomeApiFactory
import dagger.Module
import dagger.Provides

@Module
class HomeApiModule {

    @Provides
    fun provideHomeApiContract(): HomeApiContract = EmptyHomeApiFactory()
}