package com.wahyupermadie.feature_home_api.dagger

object Injector {
    var homeComponent: HomeApiComponent = DaggerHomeApiComponent.builder().build()
}