package com.wahyupermadie.feature_home.dagger
import com.wahyupermadie.feature_home.HomeMainActivity
import com.wahyupermadie.feature_home.dagger.module.HomeActivityBuilder
import com.wahyupermadie.feature_home.dagger.module.module.ExistHomeApiModule
import com.wahyupermadie.feature_home_api.dagger.scope.FeatureScope
import com.wahyupermadie.githubusershowcase.dagger.AppComponent
import dagger.Component

@FeatureScope
@Component(dependencies = [AppComponent::class], modules = [ExistHomeApiModule::class, HomeActivityBuilder::class])
interface HomeComponent {

    @Component.Builder
    interface Builder {
        fun provideAppComponent(appComponent: AppComponent): Builder
        fun build(): HomeComponent
    }

    fun inject(homeMainActivity: HomeMainActivity)
}