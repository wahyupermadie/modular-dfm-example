package com.wahyupermadie.feature_home.dagger.module

import com.wahyupermadie.feature_home.HomeMainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindsHomeMainActivity(): HomeMainActivity
}