package com.wahyupermadie.feature_home.dagger.module

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import com.wahyupermadie.feature_home.HomeMainActivity
import com.wahyupermadie.feature_home.dagger.DaggerHomeComponent
import com.wahyupermadie.githubusershowcase.MainApplication

fun HomeMainActivity.inject() {
    DaggerHomeComponent.builder()
        .provideAppComponent(MainApplication.mainComponent)
        .build()
        .inject(this)
}