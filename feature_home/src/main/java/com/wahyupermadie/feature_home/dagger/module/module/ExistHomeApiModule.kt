package com.wahyupermadie.feature_home.dagger.module.module

import com.wahyupermadie.feature_home.dagger.module.factory.ExistHomeApiFactory
import com.wahyupermadie.feature_home_api.dagger.contract.HomeApiContract
import com.wahyupermadie.feature_home_api.dagger.factory.EmptyHomeApiFactory
import dagger.Module
import dagger.Provides

@Module
object ExistHomeApiModule {

    @Provides
    fun provideHomeApiContract(): HomeApiContract = ExistHomeApiFactory()
}