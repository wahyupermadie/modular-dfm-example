package com.wahyupermadie.feature_home.dagger

import com.wahyupermadie.feature_home.dagger.module.module.ExistHomeApiModule
import com.wahyupermadie.feature_home_api.dagger.HomeApiComponent
import com.wahyupermadie.feature_home_api.dagger.contract.HomeApiContract
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ExistHomeApiModule::class])
interface ExistHomeApiComponent: HomeApiComponent {
    @Component.Builder
    interface Builder {
        fun build(): ExistHomeApiComponent
    }

    override fun homeApiContract(): HomeApiContract
}