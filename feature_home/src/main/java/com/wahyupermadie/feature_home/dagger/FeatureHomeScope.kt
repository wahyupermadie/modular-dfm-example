package com.wahyupermadie.feature_home.dagger

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FeatureHomeScope()
