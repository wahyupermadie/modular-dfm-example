package com.wahyupermadie.feature_home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.airbnb.deeplinkdispatch.DeepLink

object DeepLinkIntent {
    @JvmStatic
    @DeepLink("app://home/main")
    fun openMainHomeActivity(context: Context, extras: Bundle):Intent {
        return Intent(context, HomeMainActivity::class.java)
    }
}