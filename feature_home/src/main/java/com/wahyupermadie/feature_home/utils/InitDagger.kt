package com.wahyupermadie.feature_home.utils

import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.annotation.Keep
import com.wahyupermadie.feature_home.dagger.DaggerExistHomeApiComponent
import com.wahyupermadie.feature_home_api.dagger.Injector.homeComponent

@Keep
object InitDagger {
    init {
        homeComponent = DaggerExistHomeApiComponent.builder()
            .build()
    }
}