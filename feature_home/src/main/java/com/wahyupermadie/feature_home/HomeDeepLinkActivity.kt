package com.wahyupermadie.feature_home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.airbnb.deeplinkdispatch.DeepLinkHandler
import com.wahyupermadie.feature_home.utils.HomeDeepLinkModule
import com.wahyupermadie.feature_home.utils.HomeDeepLinkModuleRegistry

@DeepLinkHandler(HomeDeepLinkModule::class)
class HomeDeepLinkActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val deepLinkDelegate = DeepLinkDelegate(HomeDeepLinkModuleRegistry())
        deepLinkDelegate.dispatchFrom(this)
    }
}