package com.wahyupermadie.feature_profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.airbnb.deeplinkdispatch.DeepLinkHandler
import com.wahyupermadie.feature_profile.utils.ProfileDeepLinkModule
import com.wahyupermadie.feature_profile.utils.ProfileDeepLinkModuleRegistry

@DeepLinkHandler(ProfileDeepLinkModule::class)
class ProfileDeepLinkActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val deepLinkDelegate = DeepLinkDelegate(ProfileDeepLinkModuleRegistry())
        deepLinkDelegate.dispatchFrom(this)
    }
}