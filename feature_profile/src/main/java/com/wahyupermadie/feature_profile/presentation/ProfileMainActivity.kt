package com.wahyupermadie.feature_profile.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.wahyupermadie.feature_home_api.dagger.contract.HomeApiContract
import com.wahyupermadie.feature_profile.R
import com.wahyupermadie.feature_profile.dagger.inject
import javax.inject.Inject

class ProfileMainActivity : AppCompatActivity() {

    @Inject
    lateinit var homeApiContract: HomeApiContract

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_main)

        findViewById<TextView>(R.id.tvFromHomeApi).apply {
            text = homeApiContract.createStringName()
        }
    }
}