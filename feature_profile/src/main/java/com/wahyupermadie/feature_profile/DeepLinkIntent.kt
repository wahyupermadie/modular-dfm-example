package com.wahyupermadie.feature_profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.airbnb.deeplinkdispatch.DeepLink
import com.wahyupermadie.feature_profile.presentation.ProfileMainActivity

object DeepLinkIntent {

    @JvmStatic
    @DeepLink("app://profile/main")
    fun openMainProfileActivity(context: Context, extras: Bundle):Intent {
        return Intent(context, ProfileMainActivity::class.java)
    }
}