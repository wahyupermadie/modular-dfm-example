package com.wahyupermadie.feature_profile.dagger

import com.wahyupermadie.feature_home_api.dagger.HomeApiComponent
import com.wahyupermadie.feature_home_api.dagger.scope.FeatureScope
import com.wahyupermadie.feature_profile.dagger.module.ProfileActivityBuilder
import com.wahyupermadie.feature_profile.presentation.ProfileMainActivity
import com.wahyupermadie.githubusershowcase.dagger.AppComponent
import dagger.Component

@FeatureScope
@Component(dependencies = [AppComponent::class, HomeApiComponent::class], modules = [ProfileActivityBuilder::class])
interface ProfileComponent {

    @Component.Builder
    interface Builder {
        fun provideHomeApiComponent(homeApiComponent: HomeApiComponent): Builder
        fun provideAppComponent(appComponent: AppComponent): Builder
        fun build(): ProfileComponent
    }

    fun inject(profileMainActivity: ProfileMainActivity)
}