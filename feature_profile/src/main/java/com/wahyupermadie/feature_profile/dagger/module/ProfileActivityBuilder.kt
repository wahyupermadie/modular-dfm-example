package com.wahyupermadie.feature_profile.dagger.module

import com.wahyupermadie.feature_profile.presentation.ProfileMainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProfileActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindsProfileMainActivity(): ProfileMainActivity
}