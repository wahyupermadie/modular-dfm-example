package com.wahyupermadie.feature_profile.dagger

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import com.wahyupermadie.feature_home_api.dagger.Injector.homeComponent
import com.wahyupermadie.feature_profile.presentation.ProfileMainActivity
import com.wahyupermadie.githubusershowcase.MainApplication

fun ProfileMainActivity.inject() {
    DaggerProfileComponent.builder()
        .provideHomeApiComponent(homeComponent)
        .provideAppComponent(MainApplication.mainComponent)
        .build()
        .inject(this)
}