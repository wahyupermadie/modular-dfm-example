package com.wahyupermadie.feature_profile.dagger

import javax.inject.Scope
import kotlin.annotation.AnnotationRetention.RUNTIME

@Scope
@Retention(RUNTIME)
annotation class ProfileScope()
