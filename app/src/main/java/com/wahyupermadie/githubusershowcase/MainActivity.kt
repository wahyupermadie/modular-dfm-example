package com.wahyupermadie.githubusershowcase

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnProfile = findViewById<Button>(R.id.btnOpenProfile)
        val btnHome = findViewById<Button>(R.id.btnOpenHome)

        btnHome.setOnClickListener {
            Intent(Intent.ACTION_VIEW, Uri.parse("app://home/main")).run {
                startActivity(this)
            }
        }
        btnProfile.setOnClickListener {
            Intent(Intent.ACTION_VIEW, Uri.parse("app://profile/main")).run {
                startActivity(this)
            }
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }
}