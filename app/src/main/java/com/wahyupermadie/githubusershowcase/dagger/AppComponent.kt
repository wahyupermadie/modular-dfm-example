package com.wahyupermadie.githubusershowcase.dagger

import com.wahyupermadie.githubusershowcase.MainActivity
import com.wahyupermadie.githubusershowcase.MainApplication
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ActivityBuilder::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(mainApplication: MainApplication): Builder
        fun build(): AppComponent
    }

    fun inject(app: MainApplication)
    fun inject(mainActivity: MainActivity)
}