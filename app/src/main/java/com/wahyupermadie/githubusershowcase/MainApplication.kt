package com.wahyupermadie.githubusershowcase

import android.app.Application
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.airbnb.deeplinkdispatch.DeepLinkHandler
import com.wahyupermadie.githubusershowcase.dagger.AppComponent
import com.wahyupermadie.githubusershowcase.dagger.DaggerAppComponent
import com.wahyupermadie.githubusershowcase.utils.DeepLinkReceiver
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainApplication: Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    companion object {
        lateinit var mainComponent: AppComponent
    }
    override fun onCreate() {
        super.onCreate()
        mainComponent = DaggerAppComponent.builder().application(this).build()
        mainComponent.inject(this)

        startInitHomeApiDaggerComponent()
        val intentFilter = IntentFilter(DeepLinkHandler.ACTION)
        LocalBroadcastManager.getInstance(this).registerReceiver(DeepLinkReceiver(), intentFilter)
    }

    private fun startInitHomeApiDaggerComponent() {
        val initDagger = "com.wahyupermadie.feature_home.utils.InitDagger"
        try {
            val classInitDagger = Class.forName(initDagger)
            classInitDagger.newInstance()
        } catch (e: Exception) {
            e.printStackTrace();
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }
}